#pragma once
#include "Nucleus.h"
#include "Mitochondrion.h"
#include "Ribosome.h"
#include <string>
#include <iostream>

class Cell
{
private:
	Nucleus _nucleus;
	Mitochondrion _mitochondrion;
	Ribosome _ribosome;
	Gene _glocus_receptor_gene;
	unsigned int _atp_units;
public:
	bool get_ATP();
	void init(std::string dna_sequence, Gene glucose_recepotor_gene);
};