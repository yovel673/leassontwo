#pragma once
#include "Ribosome.h"
#include "Protein.h"
#include "Nucleus.h"
#include "Cell.h"

/*
	The function gets rna_transcript and creats protein.
*/
Protein* Ribosome::create_protein(std::string &RNA_transcrpit) const
{
	Protein * list = new Protein();
	list->init();
	
	std::string noc = "";
	std::string rna = RNA_transcrpit;
	unsigned int i = 0;
	bool flag = false;
	while (rna.length() >= 3 && flag == false)
	{
		noc = rna.substr(0, 3);
		rna = rna.substr(3);
		if (get_amino_acid(noc) == UNKNOWN)
		{
			std::cout << "yovel";
			list->clear();
			delete list;
			flag = true;
			list = nullptr;
		}
		else
			list->add(get_amino_acid(noc));
		
	}

	return list;
}