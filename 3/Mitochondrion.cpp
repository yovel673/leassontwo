#pragma once


#include "Mitochondrion.h"
#include "Protein.h"
#include "c:/Users/yovel/Desktop/leassontwo/AminoAcid.h"
#include <string>
#include <iostream>

/*
The Function inits the defult values to the Mitochondrion
*/
void Mitochondrion::init()
{
	_glocuse_level = 0;
	_has_glocuse_receptor = false;
}

/*
The function checks if the given protein is a cetain sequence, if it is the function changes the _has_glocuse.. to true
*/
void Mitochondrion::insert_glocuse_receptor(const Protein & protein)
{
	AminoAcidNode * aminode = protein.get_first();
	if (aminode->get_data() == ALANINE)
	{
		aminode = aminode->get_next();
		if (aminode->get_data() == LEUCINE)
		{
			aminode = aminode->get_next();
			if (aminode->get_data() == GLYCINE)
			{
				aminode = aminode->get_next();
				if (aminode->get_data() == HISTIDINE)
				{
					aminode = aminode->get_next();
					if (aminode->get_data() == LEUCINE)
					{
						aminode = aminode->get_next();
						if (aminode->get_data() == PHENYLALANINE)
						{
							aminode = aminode->get_next();
							if (aminode->get_data() == AMINO_CHAIN_END)
							{
								_has_glocuse_receptor = true;
							}
						}
					}
				}
			}
		}
	}
	
}

/*
The function sets the glocuse level to the given value
*/
void Mitochondrion::set_glocuse(const unsigned int glocuse_units)
{
	_glocuse_level = glocuse_units;
}

/*
The function checks if the Mitochondrion is able to produce ATP
*/
bool Mitochondrion::produceATP() const
{
	if ((_has_glocuse_receptor) && (_glocuse_level >= 50))
	{
		return true;
	}
	return false;
}