#pragma once
#include "Cell.h"
#include "Nucleus.h"
#include "Mitochondrion.h"
#include "Ribosome.h"
#include <string>
#include <iostream>

/*
The function inits the given inputs to the cell and inits mitochondrion
*/
void Cell::init(std::string dna_sequence, Gene glucose_recepotor_gene)
{
	_nucleus.init(dna_sequence);
	_glocus_receptor_gene = glucose_recepotor_gene;

	_mitochondrion.init();
}


/*
The function checks if can produce ATP
*/
 bool Cell::get_ATP()
{
	 std::string rna = _nucleus.get_RNA_transcript(_glocus_receptor_gene);

	 if (_ribosome.create_protein(rna) == nullptr)
	 {
		 std::cerr << "error!! cant make protein!!";
		 _exit(1);
	 }

	 _mitochondrion.insert_glocuse_receptor(*_ribosome.create_protein(rna));
	 _mitochondrion.set_glocuse(100);

	 if (_mitochondrion.produceATP())
	 {
		 _atp_units = 100;
		 return true;
	 }
	 return false;


}
