#pragma once
#include "Nucleus.h"
#include <iostream>
#include <algorithm>
#include <string>

/*
Input: unsignd int start
usigned int end
bool on_complementary_dna_starnd

The function puts the inputs into the class variables.
*/
void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	_start = start;
	_end = end;
	_on_complementary_dna_strand = on_complementary_dna_strand;
}

/*
The function return True/False by the status of _on_complementary_dna_starnd.
*/
bool Gene::is_on_complementary_dna_strand() const
{
	return _on_complementary_dna_strand;
}

/*
The function returns the gene's start
*/
unsigned int Gene::get_start() const
{
	return _start;
}

/*
The function returns the gene's end
*/
unsigned int Gene::get_end() const
{
	return _end;
}

/*
The function setting the gene's start to the input value
*/
void Gene::SetStart(const unsigned int start)
{
	_start = start;
}

/*
The function setting the gene's end to the input value
*/
void Gene::SetEnd(const unsigned int end)
{
	_end = end;
}

/*
The function setting the gene's _on_comp... to the input value
*/
void Gene::Set_On_comp_dna_starnd(const bool on_complementary_dna_strand)
{
	_on_complementary_dna_strand = on_complementary_dna_strand;
}


/*
The function gets dna_sequence string and builds matching DNA starnd.
if the input is invalid(contains charachter that is not A,T,C,G the function exits.
*/
void Nucleus::init(const std::string dna_sequence)
{
	_DNA_starnd = dna_sequence;
	int i = 0;
	char letter = 'a';
	std::string comp_starnd = "";
	for (i = 0;dna_sequence[i] != '\0' ; i++)
	{
		letter = dna_sequence[i];
		if (letter == 'A')
			comp_starnd.append("T");
		else if (letter == 'G')
			comp_starnd.append("C");
		else if (letter == 'C')
			comp_starnd.append("G");
		else if (letter == 'T')
			comp_starnd.append("A");
		else
		{
			std::cerr << "the sequnce is invalid";
			_exit(0);
		}
	}
	
	_complementary_DNA_strand = comp_starnd;

}


/*
The function gets Gene and returns the right rna trascript
*/
std::string Nucleus::get_RNA_transcript(const Gene& gene)const
{
	int start = gene.get_start();
	int end = gene.get_end();
	bool on_comp = gene.is_on_complementary_dna_strand();
	int len = (end - start) + 2;
	std::string RNA = "";

	if (on_comp)
		RNA = _complementary_DNA_strand.substr(start, len);
	else
		RNA = _DNA_starnd.substr(start, len);
	
	std::replace(RNA.begin(), RNA.end(), 'T', 'U');
	return RNA;
	
}


/*
The function reversing the DNA starnd
*/
std::string Nucleus::get_reversed_DNA_strand()const
{
	std::string DNA = _DNA_starnd;
	std::reverse(DNA.begin(), DNA.end());
	return DNA;
}

/*
The function gets a codon(3 dna letters squence) and returns how many times it appears on the dna starnd
*/
unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const
{
	unsigned int count = 0;
	std::string::size_type pos = 0;
	std::string s = _DNA_starnd;
	std::string target = codon;

	while ((pos = s.find(target, pos)) != std::string::npos) {
		++count;
		pos += target.length();
	}

	return count;
}
